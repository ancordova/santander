/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uchile.wic.santander;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import org.postgresql.util.PGobject;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author ricoc_000
 */
public class DSA {

    // Credenciales   
    private static String Ckey = "8B78rPejHGAdNbArYRn2tPpVw";
    private static String CkeySecret = "U8rP92ONHRIoP82JTMNsmuuuqFR3F9eFXRDrn73lWBUUNoAILF";
    private static String AToken = "409619735-zyatT1vf09BwNGxlOaEhqeU9YauPVZbn7Q1GqMTS";
    private static String AtokenSecret = "3GVVhYedKk2Qc4pj0R0A5JSc0ooom69Fw9fM7I9eO1tHT";

    // ConexiÃ³n con la base de datos
    private static Connection conn = Helpers.GetConnectionPostgresql("santander");
    private static PreparedStatement insertar;
    private static String sql;

    //main
    public static void main(String[] args) {

        // Configuration 
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(Ckey)
                .setOAuthConsumerSecret(CkeySecret)
                .setOAuthAccessToken(AToken)
                .setOAuthAccessTokenSecret(AtokenSecret)
                .setJSONStoreEnabled(true);

        Twitter twitter = new TwitterFactory(cb.build()).getInstance();

        try {

            Query query = new Query("santanderchile");
            QueryResult result;
            int Count = 0;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                    JSONObject jobject = new JSONObject(TwitterObjectFactory.getRawJSON(tweet));
                    JSONObject juser = jobject.getJSONObject("user");

                    java.util.Date date = new java.util.Date();
                    Timestamp timestamp = new Timestamp(date.getTime());

                    try {
                        PGobject dataTweet = new PGobject();
                        dataTweet.setType("jsonb");
                        dataTweet.setValue(jobject.toString());

                        PGobject dataUser = new PGobject();
                        dataUser.setType("jsonb");
                        dataUser.setValue(jobject.toString());

                        sql = "INSERT INTO dsa_tweet ( data, snap) "
                                + "VALUES (?,?)";
                        insertar = conn.prepareStatement(sql);
                        insertar.setObject(1, dataTweet);
                        insertar.setTimestamp(2, timestamp);
                        insertar.execute();

                        sql = "INSERT INTO dsa_user ( data, snap) "
                                + "VALUES (?,?)";
                        insertar = conn.prepareStatement(sql);
                        insertar.setObject(1, dataUser);
                        insertar.setTimestamp(2, timestamp);
                        insertar.execute();

                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Count++;
                }
            } while ((query = result.nextQuery()) != null);
            System.out.println("Tweets revisados: "+Count);
            System.exit(0);

        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
            System.exit(-1);
        }
    }
}
