/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uchile.wic.santander;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

/**
 *
 * @author ricoc_000
 */
public class Helpers {

    public static Connection GetConnectionPostgresql(String database) {
        Connection con = null;
        String Host = "jdbc:postgresql://localhost:5432/" + database;
        String User = "postgres";
        String Pass = "SalitaSur1";

        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(Host, User, Pass);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.toString());
            System.out.println("Falla al conectar a PostgreSQL");
        }
        return con;
    }

    static double GetPolarityFromSentenceAsRandom(String sentence) {
        double polarity = (Math.random() - 0.5) * 10;
        return polarity;
    }

    static double GetPolarityFromCorpus(String sentence) throws FileNotFoundException, IOException {
        double polaridad = 0;
        String frase = preprocesar(sentence);
        BufferedReader positivas = new BufferedReader(new FileReader("positivas.txt"));
        BufferedReader negativas = new BufferedReader(new FileReader("negativas.txt"));
        String line = null;
        while ((line = positivas.readLine()) != null) {
            if (frase.contains(line.toLowerCase())) polaridad = polaridad + 2;
        }
        while ((line = negativas.readLine()) != null) {
            if (frase.contains(line.toLowerCase())) polaridad = polaridad - 2;
        }
        return polaridad;
    }
    
    static int GetSexo(String name) throws FileNotFoundException, IOException {
        String nombre = preprocesar(name);
        BufferedReader hombres = new BufferedReader(new FileReader("ninos.txt"));
        BufferedReader mujeres = new BufferedReader(new FileReader("ninas.txt"));
        String line = null;
        while ((line = hombres.readLine()) != null) {
            if (nombre.contains(line.toLowerCase())) return 1; //hombre
        }
        while ((line = mujeres.readLine()) != null) {
            if (nombre.contains(line.toLowerCase())) return 2; //mujer
        }
        return 3; // 3: indefinido
    }
    
    /**
     * Funcion para devolver el sexo por nombre de registro civil con dos
     * parámetros. Ocupa la función GetSexo(String name)
     *
     * @param String name
     * @return 1: hombre, 2: mujer, 3: indefinido
     * @since v0.02
     */
    static int GetSexo(String name, String screenname) throws FileNotFoundException, IOException {
        int sexo = GetSexo(name);
        if (sexo == 3) {
            sexo = GetSexo(screenname);
        }
        return sexo;
    }

    static public String preprocesar(String sentence) {
        String frase = sentence.toLowerCase();
        frase = frase.replaceAll("á", "a");
        frase = frase.replaceAll("é", "e");
        frase = frase.replaceAll("í", "i");
        frase = frase.replaceAll("ó", "o");
        frase = frase.replaceAll("ú", "u");
        return frase;
    }
}
