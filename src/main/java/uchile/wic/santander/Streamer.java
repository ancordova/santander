package uchile.wic.santander;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.json.JSONObject;
import org.postgresql.util.PGobject;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class Streamer extends Thread {

    // Credenciales   
    private static String Ckey = "8B78rPejHGAdNbArYRn2tPpVw";
    private static String CkeySecret = "U8rP92ONHRIoP82JTMNsmuuuqFR3F9eFXRDrn73lWBUUNoAILF";
    private static String AToken = "409619735-zyatT1vf09BwNGxlOaEhqeU9YauPVZbn7Q1GqMTS";
    private static String AtokenSecret = "3GVVhYedKk2Qc4pj0R0A5JSc0ooom69Fw9fM7I9eO1tHT";

    // ConexiÃ³n con la base de datos
    private static  Connection conn = Helpers.GetConnectionPostgresql("santander");
    private static PreparedStatement insertar;
    private static String sql;

    // Main
    public static void main(String[] args) throws TwitterException {
        
        // Configuration 
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(Ckey)
                .setOAuthConsumerSecret(CkeySecret)
                .setOAuthAccessToken(AToken)
                .setOAuthAccessTokenSecret(AtokenSecret)
                .setJSONStoreEnabled(true);
        
        // Strem y listener
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        StatusListener listener = new StatusListener() {
            
            // onStatus se encarga de hacer cosas con lo recibido
            @Override
            public void onStatus(Status status) {
                try {
                //System.out.println(jsons.toString());
                java.util.Date date = new java.util.Date();
                Timestamp timestamp = new Timestamp(date.getTime());
                
                PGobject dataObject = new PGobject();
                JSONObject jobject = new JSONObject(TwitterObjectFactory.getRawJSON(status));
                dataObject.setType("jsonb");
                dataObject.setValue(jobject.toString());
                
                sql = "INSERT INTO tweet (data, timestamp) "
                        + "VALUES (?,?)";
                insertar = conn.prepareStatement(sql);
                insertar.setObject(1, dataObject);
                insertar.setTimestamp(2, timestamp);
                insertar.execute();
                } catch (SQLException sqle){
                    sqle.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onScrubGeo(long arg0, long arg1) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStallWarning(StallWarning arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTrackLimitationNotice(int arg0) {
                // TODO Auto-generated method stub
            }
        };

        twitterStream.addListener(listener);
        FilterQuery filtre = new FilterQuery();
        filtre.track("santanderchile", "DFinanciero", "sbif", "bbvachile", "bancodechile", "bcentralchile");
        twitterStream.filter(filtre);

    }
}
