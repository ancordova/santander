/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uchile.wic.santander;

import java.util.TimeZone;
import static org.quartz.CronScheduleBuilder.*;
import static org.quartz.JobBuilder.*;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.*;
import org.quartz.impl.StdSchedulerFactory;
/**
 *
 * @author ricoc_000
 */
public class Automatico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Creacion de una instacia de Scheduler
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            System.out.println("Iniciando Scheduler...");
            scheduler.start();
            
            // Seconds | Minutes | Hours | Day-of-Month | Month | Day-of-Week | Year (optional field)
            
            JobDetail extractor = newJob(Extractor.class).withIdentity("extractor").build();            
            Trigger triggerextractor = newTrigger().withIdentity("triggerextractor").startNow()
                    .forJob("extractor").withSchedule(cronSchedule("00 59/2 * * * ?")
                    .inTimeZone(TimeZone.getTimeZone("America/Chile/Santiago"))).build();
            
            scheduler.scheduleJob(extractor, triggerextractor);
            

        } catch (Exception e) { 
            e.printStackTrace();
        }
    }
    
}
